var date;
var day = Math.random()*12+1;   //получаем случайный месяц
date = Math.floor(day);
if(date<10) date = "0"+date;

if(date==2) day = Math.floor(Math.random()*28+1); //получаем случайную дату в феврале
else if( (date%2==0 && date<=7) || (date%2==1 && date>7) )
    day = Math.floor(Math.random()*30+1);   //получаем случайную дату в месяце с 30 днями
else if ( (date%2==1 && date<=7) || (date%2==0 && date>7) )
    day = Math.floor(Math.random()*31+1);   //получаем случайную дату в месяце с 31 днем

if(day<10) day = "0"+day;
date=day+"."+date;  //дата в формате "00.00"
console.log(date);

//для ручного ввода
//date="08.03";
//console.log(date);

switch (date){
    case "08.03":
        console.log('с 8 марта!');
        break;
    case "23.02":
        console.log('с 23 февраля');
        break;
    case "31.12":
        console.log('с новым годом');
        break;
    default:
        console.log('сегодня просто отличный день и безо всякого праздника');
}